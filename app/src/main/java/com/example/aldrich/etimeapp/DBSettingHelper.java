package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Administrator on 2016/4/23.
 */
public class DBSettingHelper extends Activity {
    DBOpenHelper1 dbOpenHelper;//initialize a OpenHelper1
    public DBSettingHelper(Context context){
        this.dbOpenHelper=new DBOpenHelper1(context);
    }
    /**
     * @param setting
     */
    //method for initializing the setting table in database
    public void addDefaultSetting(SettingClass setting){
        SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("pertime", setting.getPertime());
        cv.put("frequency", setting.getFrequency());
        cv.put("alarm", setting.getAlarm());
        /* insert data */
        db.insert("setting", null, cv);
        db.close();
    }
    // save setting to database
    public void save(SettingClass setting){
        SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("pertime", setting.getPertime());
        cv.put("frequency", setting.getFrequency());
        cv.put("alarm", setting.getAlarm());
        // insert data
        db.update("setting", cv, "settingID" + " = 1 " , null);
        db.close();
    }

    /**
     * @param
     */

    //this can be used for searching, but the code need esome change so it can return many events with same key words
    public SettingClass getSetting(){
        SQLiteDatabase db=dbOpenHelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select * from setting where settingID=?", new String[]{"1"});
        if(cursor.moveToFirst()){
            int uid2=cursor.getInt(cursor.getColumnIndex("settingID"));
            String pertime=cursor.getString(cursor.getColumnIndex("pertime"));
            String frequency=cursor.getString(cursor.getColumnIndex("frequency"));
            String alarm=cursor.getString(cursor.getColumnIndex("alarm"));
            SettingClass setting = new SettingClass();
            setting.setID(uid2);
            setting.setPertime (pertime);
            setting.setFrequency(frequency);
            setting.setAlarm(alarm);
            return setting;
        }
        cursor.close();
        return null;
    }

    public long getCount(){ //how much event there currently are
        SQLiteDatabase db=dbOpenHelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select count(*) from setting", null);
        cursor.moveToFirst();
        long reslut=cursor.getLong(0);
        return reslut; //return the number
    }


}
