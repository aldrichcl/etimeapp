package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2016/4/23.
 */
public class DBOpenHelper1 extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "etimeDatabase";

    // Tables name
    private static final String TABLE_EVENT = "event";
    private static final String TABLE_CATEGORY = "category";
    private static final String TABLE_SETTING = "setting";

    // Event Column Names
    private static final String EVENT_ID = "eventID";
    private static final String EVENT_NAME = "name";
    private static final String EVENT_DESCRIPTION = "description";
    private static final String EVENT_STARTDATE = "startdate";
    private static final String EVENT_STARTTIME= "starttime";
    private static final String EVENT_UPORPAST= "uporpast";
    private static final String EVENT_CATEGORY= "categoryID";
    private static final String EVENT_ALARM_ID= "alarmID";
    private static final String EVENT_PERTIME = "pertime";
    private static final String EVENT_FREQUENCY = "frequency";

    // Category Column Names
    private static final String CATEGORY_ID = "categoryID";
    private static final String CATEGORY_NAME = "categoryname";

    // Setting Column Names
    private static final String SETTING_ID = "settingID";
    private static final String SETTING_PERTIME = "pertime";
    private static final String SETTING_FREQUENCY = "frequency";
    private static final String SETTING_ALARM = "alarm";

    // Create Statements
    private static final String CREATE_TABLE_EVENT = "create table "
            + TABLE_EVENT + "(" + EVENT_ID + " integer primary key autoincrement, "
            + EVENT_NAME + " text, " + EVENT_DESCRIPTION + " text, "
            + EVENT_PERTIME + " text, " + EVENT_FREQUENCY + " integer, "
            + EVENT_STARTDATE + " text, " + EVENT_STARTTIME + " text, "
            + EVENT_UPORPAST + " integer, " + EVENT_ALARM_ID + " text, " + EVENT_CATEGORY + " integer, "+
            "CONSTRAINT FK_CATEGORY_ID FOREIGN KEY (categoryID) REFERENCES category (category_ID)"+" );";

    private static final String CREATE_TABLE_CATEGORY = "create table "
            + TABLE_CATEGORY + "(" + CATEGORY_ID + " integer primary key autoincrement, "
            + CATEGORY_NAME + " text);";

    private static final String CREATE_TABLE_SETTING = "create table "
            + TABLE_SETTING + "(" + SETTING_ID + " integer primary key autoincrement, "
            + SETTING_PERTIME + " text, " + SETTING_FREQUENCY + " text, "
            + SETTING_ALARM + " text);";

    public DBOpenHelper1(Context context) {
        super(context, "TestDatabaseV6.db", null, 1);
    }


    //this will be done when the database is created，if the databse already exist, it won't execute
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_SETTING);
        db.execSQL(CREATE_TABLE_CATEGORY);
        db.execSQL(CREATE_TABLE_EVENT);
    }

    //database version
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTING);

        // Creating new tables
        onCreate(db);
    }
}
