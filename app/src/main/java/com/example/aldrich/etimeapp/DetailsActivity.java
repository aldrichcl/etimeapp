package com.example.aldrich.etimeapp;



/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.speech.RecognizerIntent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;


public class DetailsActivity extends ActionBarActivity {


    DBEventHelper dbEventHelper = new DBEventHelper(this);

    EventClass eventE=new EventClass();

    String evidDetail;

    Cursor cursor = null;

    TextView textLabelTitle;
    TextView textLabelDescription;
    TextView textLabelDate;
    TextView textLabelCategory;
    TextView txtTitleE;
    TextView txtDesE;
    TextView preTimeText;
    TextView frequencyText;
    Button btnStartTimeE;
    Button btnStartDateE;
    Spinner spn_category;
    Spinner spn_pretimeE;
    Spinner spn_frequencyE;
    Button btnEditE;
    Button btnSaveE;
    Button btnResetE;
    Button btnRemoveE;
    Button voiceBtn;
    Button voiceDescBtn;

    Button btnVoiceE;
    Button btnVoiceDescriptionE;

    String[] dateE;
    String[] timeE;


    String alarmID;

    private AlarmManager am1 = null; // created the alarmManager
    private AlarmManager am2  = null; // created the alarmManager
    Calendar now = Calendar.getInstance();
    Calendar selectedTimeE = now;


    int year_startE, month_startE, day_startE;
    int hour_startE, minute_startE;

    int daysOfPretimeE; //how nay days pf pretime setting
    int hoursOfFrequencyE;//how nay days pf frequency setting

    final int REQ_CODE_SPEECH_INPUT = 1;
    final int REQ_CODE_SPEECH_DESCRIPTION = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Set to support only Portrait

        // Get a SharedPreferences and its values
        final SharedPreferences preferences = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        final int theme = preferences.getInt("theme", 0);
        final int language = preferences.getInt("language", 0);

        // Set the language for the activity
        if (language == 0){
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            setTitle("Details");
        }
        else if (language == 1){
            String languageToLoad  = "zh";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("详情");
        }
        else if(language == 2){
            String languageToLoad  = "vi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Chi tiết");
        }
        else if (language == 3){
            String languageToLoad  = "in";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Rincian");
        }
        else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Details");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Initalise widgets
        textLabelTitle = (TextView) findViewById(R.id.lblTit);
        textLabelDescription= (TextView) findViewById(R.id.lblDes);
        textLabelDate = (TextView) findViewById(R.id.textView2);
        textLabelCategory = (TextView) findViewById(R.id.textView8);
        txtTitleE = (TextView) findViewById(R.id.txtTitleE);
        txtDesE = (TextView) findViewById(R.id.txtDesE);
        btnStartTimeE = (Button) findViewById(R.id.btnStartTimeE);
        btnStartDateE = (Button) findViewById(R.id.btnStartDateE);
        spn_category = (Spinner) findViewById(R.id.spnCategoryE);
        spn_frequencyE=(Spinner) findViewById(R.id.spnFrequencyE);
        spn_pretimeE=(Spinner) findViewById(R.id.spnPreTimeE);
        btnEditE=(Button)findViewById(R.id.btnEditE);
        btnSaveE=(Button)findViewById(R.id.btnSaveE);
        btnResetE=(Button)findViewById(R.id.btnResetE);
        btnRemoveE=(Button)findViewById(R.id.btnRemove);
        voiceBtn = (Button) findViewById(R.id.btnVoiceE);
        voiceDescBtn = (Button) findViewById(R.id.btnVoiceDescriptionE);
        preTimeText = (TextView) findViewById(R.id.textView);
        frequencyText = (TextView) findViewById(R.id.textView3);
        btnVoiceE=(Button)findViewById(R.id.btnVoiceE);
        btnVoiceDescriptionE=(Button)findViewById(R.id.btnVoiceDescriptionE);

        // Set the theme to user's choice or default
        if (theme == 0){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);

            btnStartDateE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnStartTimeE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnEditE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnSaveE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnResetE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnRemoveE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            voiceBtn.setBackground(getResources().getDrawable(R.drawable.designbutton));
            voiceDescBtn.setBackground(getResources().getDrawable(R.drawable.designbutton));
        }
        else if (theme == 1){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.natural_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.natural_background_color));

            textLabelTitle.setTextColor(getResources().getColor(R.color.natural_main_color));
            textLabelDescription.setTextColor(getResources().getColor(R.color.natural_main_color));
            textLabelDate.setTextColor(getResources().getColor(R.color.natural_main_color));
            textLabelCategory.setTextColor(getResources().getColor(R.color.natural_main_color));
            preTimeText.setTextColor(getResources().getColor(R.color.natural_main_color));
            frequencyText.setTextColor(getResources().getColor(R.color.natural_main_color));

            btnStartDateE.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnStartTimeE.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnEditE.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnSaveE.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnResetE.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnRemoveE.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            voiceBtn.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            voiceDescBtn.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
        }
        else if (theme == 2){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.dark_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.dark_background_color));

            textLabelTitle.setTextColor(getResources().getColor(R.color.white_color));
            textLabelTitle.setTextColor(getResources().getColor(R.color.white_color));
            textLabelDescription.setTextColor(getResources().getColor(R.color.white_color));
            textLabelDate.setTextColor(getResources().getColor(R.color.white_color));
            textLabelCategory.setTextColor(getResources().getColor(R.color.white_color));
            preTimeText.setTextColor(getResources().getColor(R.color.white_color));
            frequencyText.setTextColor(getResources().getColor(R.color.white_color));

            btnStartDateE.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnStartTimeE.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnEditE.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnSaveE.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnResetE.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnRemoveE.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            voiceBtn.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            voiceDescBtn.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);

            btnStartDateE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnStartTimeE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnEditE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnSaveE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnResetE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            btnRemoveE.setBackground(getResources().getDrawable(R.drawable.designbutton));
            voiceBtn.setBackground(getResources().getDrawable(R.drawable.designbutton));
            voiceDescBtn.setBackground(getResources().getDrawable(R.drawable.designbutton));
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true); // Support a back button to previous Activity

        //get the eventid on the main page
        evidDetail =getIntent().getStringExtra("eventid");
        //check the value
        if(evidDetail=="") {
            //return back to the list recipe if null value
            Intent intent = new Intent(DetailsActivity.this, MainActivity.class);
            startActivity(intent);
        }
        else {

            cursor=dbEventHelper.getCursoronEventID(Integer.parseInt( evidDetail));

            do{

                //set the values from the database to the controllers and the object event
                //the eventE use for reset the value of the controllers
                txtTitleE.setText(cursor.getString(cursor.getColumnIndex("name")));
                eventE.setName(cursor.getString(cursor.getColumnIndex("name")));
                txtDesE.setText(cursor.getString(cursor.getColumnIndex("description")));
                eventE.setDescription(cursor.getString(cursor.getColumnIndex("description")));

                timeE=cursor.getString(cursor.getColumnIndex("starttime")).split(":");


                btnStartTimeE.setText(timeE[0] + ":" + timeE[1]);
                eventE.setStartTime(timeE[0] + ":" + timeE[1]);
                hour_startE=Integer.parseInt( timeE[0]);
                minute_startE=Integer.parseInt( timeE[1]);

                //check is where the event come from? in upcoming list or past list
                if(cursor.getInt(cursor.getColumnIndex("uporpast"))==0){
                    btnEditE.setVisibility(View.INVISIBLE);
                }

                dateE=cursor.getString(cursor.getColumnIndex("startdate")).split("/");

                btnStartDateE.setText(dateE[0] + "/" + dateE[1] + "/" + dateE[2]);
                eventE.setStartDate(dateE[0] + "/" + dateE[1] + "/" + dateE[2]);
                year_startE=Integer.parseInt(dateE[0]);
                month_startE=Integer.parseInt(dateE[1]);
                day_startE=Integer.parseInt(dateE[2]);

                spn_category.setSelection(cursor.getInt(cursor.getColumnIndex("categoryID")));
                eventE.setCategoryID(cursor.getInt(cursor.getColumnIndex("categoryID")));
                spn_frequencyE.setSelection(cursor.getInt(cursor.getColumnIndex("frequency")));
                eventE.setFrequency(cursor.getInt(cursor.getColumnIndex("frequency")));
                spn_pretimeE.setSelection(cursor.getInt(cursor.getColumnIndex("pertime")));
                eventE.setPretime(cursor.getInt(cursor.getColumnIndex("pertime")));

                alarmID=String.valueOf(cursor.getString(cursor.getColumnIndex("alarmID")));
                eventE.setAlarmID(cursor.getString(cursor.getColumnIndex("alarmID")));
                eventE.setID(Integer.parseInt(evidDetail));

            } while (cursor.moveToNext());
            cursor.close();

        SetControls(false);
            btnSaveE.setVisibility(View.INVISIBLE);
            btnResetE.setVisibility(View.INVISIBLE);
            btnRemoveE.setVisibility(View.VISIBLE);

            showDialogOnButtonClick();

            btnVoiceE.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    promptSpeechInput();
                }
            });

            btnVoiceDescriptionE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    promptSpeechInputDescription();
                }
            });

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void btnSave(View v)
    {

        if(ifFieldsFilled()==false){
            // do nothing
        }
        else if(ifPretimeValid()==false){
            //do nothing
        }
        else{

            now = Calendar.getInstance();

            String EventControllerID=String.valueOf(Integer.parseInt( eventE.getAlarmID())+1);
            Log.d("qqqqqqqqqq", eventE.getAlarmID());
            Log.d("qqqqqqqqqq", EventControllerID);



            //add data into event (name,description etc.)
            eventE.setName(txtTitleE.getText().toString());
            eventE.setDescription(txtDesE.getText().toString());
            eventE.setStartDate(btnStartDateE.getText().toString());
            eventE.setStartTime(btnStartTimeE.getText().toString());
            eventE.setStatus(true);  //set to upcoming event

            eventE.setCategoryID(spn_category.getSelectedItemPosition());
            eventE.setPretime(spn_pretimeE.getSelectedItemPosition());
            eventE.setFrequency(spn_frequencyE.getSelectedItemPosition());


            ///Set the alarmManager
            selectedTimeE = Calendar.getInstance(); //first initiate it so the second and millisecond is same with now
            //c.setTimeInMillis(System.currentTimeMillis());
            selectedTimeE.set(Calendar.YEAR, year_startE);
            selectedTimeE.set(Calendar.MONTH, month_startE-1);
            selectedTimeE.set(Calendar.DAY_OF_MONTH, day_startE);
            selectedTimeE.set(Calendar.HOUR_OF_DAY, hour_startE);
            selectedTimeE.set(Calendar.MINUTE, minute_startE);
            selectedTimeE.set(Calendar.SECOND, 0);
            selectedTimeE.set(Calendar.MILLISECOND, 0);

            if(DataValidation()==false){
                //do nothing
            }
            else{
                //get arguments we need
                long alarmStartTime = selectedTimeE.getTimeInMillis() - daysOfPretimeE*24*60*60*1000;
                long alarmRepeatTime = hoursOfFrequencyE * 60* 60 *1000;

                RemoveAlarmEvent();



                //set alarm which is repetitive;
                Intent intent_Alarm = new Intent(DetailsActivity.this, AlarmReceiver.class);
                intent_Alarm.putExtra("alarmID", eventE.getAlarmID());
                intent_Alarm.putExtra("eventName", eventE.getName());
                intent_Alarm.putExtra("eventTime", getSelectedTime());
                intent_Alarm.putExtra("eventTimeInMillis", selectedTimeE.getTimeInMillis()); //long
                intent_Alarm.putExtra("eventFrequency", alarmRepeatTime); //long
                intent_Alarm.putExtra("count", 0);
                PendingIntent pi = PendingIntent.getBroadcast(DetailsActivity.this, Integer.parseInt(eventE.getAlarmID()), intent_Alarm,PendingIntent.FLAG_UPDATE_CURRENT );
                am1.setExact(AlarmManager.RTC_WAKEUP, alarmStartTime, pi);

                //event and corresponding alarm will be calceled and removed to history event
                //update event to database
                dbEventHelper.updateEventByeventID(eventE);


                Toast.makeText(DetailsActivity.this, getResources().getString(R.string.savesuccessfully_text),Toast.LENGTH_SHORT).show();


                btnEditE.setVisibility(View.VISIBLE);
                btnSaveE.setVisibility(View.INVISIBLE);
                btnResetE.setVisibility(View.INVISIBLE);
                SetControls(false);

            }


        }


    }
//disable or enable buttons
    public void btnEdit(View v)
    {
        SetControls(true);
        btnEditE.setVisibility(View.INVISIBLE);
        btnSaveE.setVisibility(View.VISIBLE);
        btnResetE.setVisibility(View.VISIBLE);



    }
//set all controllers become the beginning values
    public void btnResetE(View v){
        txtTitleE.setText(eventE.getName());
        txtDesE.setText(eventE.getDescription());

        btnStartTimeE.setText(eventE.getStartTime());
        timeE=eventE.getStartTime().split(":");
        hour_startE=Integer.parseInt(timeE[0]);
        minute_startE=Integer.parseInt(timeE[1]);


        btnStartDateE.setText(eventE.getStartDate());
        dateE=eventE.getStartDate().split("/");
        year_startE=Integer.parseInt(dateE[0]);
        month_startE=Integer.parseInt(dateE[1]);
        day_startE=Integer.parseInt(dateE[2]);

        spn_category.setSelection(eventE.getCategoryID());
        spn_frequencyE.setSelection(eventE.getFrequency());
        spn_pretimeE.setSelection(eventE.getPretime());
    }
//enable or disable the input controllers
    public void SetControls(boolean value)
    {
        txtTitleE.setEnabled(value);
        txtDesE.setEnabled(value);
        btnStartTimeE.setEnabled(value);
        btnStartDateE.setEnabled(value);
        spn_category.setEnabled(value);
        spn_frequencyE.setEnabled(value);
        spn_pretimeE.setEnabled(value);
        btnVoiceE.setEnabled(value);
        btnVoiceDescriptionE.setEnabled(value);
    }

    public void showDialogOnButtonClick() {


        btnStartDateE.setOnClickListener(

                new View.OnClickListener() {
                    Calendar now = Calendar.getInstance();

                    @Override
                    public void onClick(View v) {
                        new DatePickerDialog(DetailsActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        year_startE = year;
                                        month_startE = monthOfYear + 1;
                                        day_startE = dayOfMonth;


                                        btnStartDateE.setText(year_startE + "/" + month_startE + "/" + day_startE);
                                    }
                                }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)).show();

                    }
                });


        btnStartTimeE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog a = new TimePickerDialog(DetailsActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                hour_startE = hourOfDay;
                                minute_startE = minute;
                                btnStartTimeE.setText(hour_startE + ":" + minute_startE);
                            }
                        }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                a.show();
            }
        });
    }
//remove event
    public void btnRemove(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getResources().getString(R.string.confirm_text));
        builder.setMessage(getResources().getString(R.string.delete_confirm_text));

        builder.setPositiveButton(getResources().getString(R.string.yes_text), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                RemoveAlarmEvent();


                dbEventHelper.deleteEvent(Integer.parseInt(evidDetail));
                finish();
                Intent intent = new Intent(DetailsActivity.this, MainActivity.class);
                startActivity(intent);
                finish();


                dialog.dismiss();
            }

        });

        builder.setNegativeButton(getResources().getString(R.string.cancel_text), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

//remove alarm in event
    public void RemoveAlarmEvent(){
        // Do nothing but close the dialog
        Intent intent_Alarm = new Intent(DetailsActivity.this, AlarmReceiver.class);

        PendingIntent pi = PendingIntent.getBroadcast(DetailsActivity.this, Integer.parseInt(eventE.getAlarmID()), intent_Alarm, PendingIntent.FLAG_CANCEL_CURRENT);

        am1 = (AlarmManager) getSystemService(ALARM_SERVICE);
        am1.cancel(pi);


        String EventControllerID=String.valueOf(Integer.parseInt( eventE.getAlarmID())+1);

        Intent intent_controller = new Intent(DetailsActivity.this, AlarmReceiver.class);
        intent_controller.setAction(EventControllerID);
        PendingIntent pi_controller = PendingIntent.getBroadcast(DetailsActivity.this, Integer.parseInt(EventControllerID), intent_controller, PendingIntent.FLAG_CANCEL_CURRENT);
        am2 = (AlarmManager) getSystemService(ALARM_SERVICE);
        am2.cancel(pi_controller);


    }

    public boolean ifPretimeValid(){ //check if pretime > frequency
        daysOfPretimeE  = 0;
        if(spn_pretimeE.getSelectedItem().equals("1 month")){ daysOfPretimeE=30;}
        else if(spn_pretimeE.getSelectedItem().equals("2 weeks")){daysOfPretimeE=14;}
        else if(spn_pretimeE.getSelectedItem().equals("1 week")){daysOfPretimeE=7;}
        else if(spn_pretimeE.getSelectedItem().equals("3 days")){daysOfPretimeE=3;}
        else if(spn_pretimeE.getSelectedItem().equals("1 day")){ daysOfPretimeE=1;}
        else if(spn_pretimeE.getSelectedItem().equals("Today")){daysOfPretimeE=0; return true;}


        hoursOfFrequencyE = 0;
        if(spn_frequencyE.getSelectedItem().equals("weekly")){ hoursOfFrequencyE=7*24;}
        else if(spn_frequencyE.getSelectedItem().equals("3 days")){ hoursOfFrequencyE=3*24;}
        else if(spn_frequencyE.getSelectedItem().equals("1 day")){hoursOfFrequencyE=1*24;}
        else if(spn_frequencyE.getSelectedItem().equals("12 hours")){ hoursOfFrequencyE=12;}

        if(daysOfPretimeE * 24 < hoursOfFrequencyE ){ //if it's a valid input
            Toast.makeText(DetailsActivity.this, "Event time must be after pre-time from now.", Toast.LENGTH_LONG).show();
            return  false;
        }
        else return true;
    }

    public boolean ifFieldsFilled(){
        if(txtTitleE.getText().toString().equals((""))){
            Toast.makeText(DetailsActivity.this, getResources().getString(R.string.forgetfields_text), Toast.LENGTH_LONG).show();
            return false;
        }
        else if(btnStartTimeE.getText().toString().equals (getResources().getString(R.string.selecttime_text)) || btnStartDateE.getText().toString().equals (getResources().getString(R.string.selectdate_text))){//if user hasn't select a time at all
            Toast.makeText(DetailsActivity.this, getResources().getString(R.string.forgetdate_text), Toast.LENGTH_LONG).show();
            return false;
        }
        else return true;
    }

    public boolean DataValidation(){

        if(now.compareTo(selectedTimeE)>=0){ //if the day set already past
            Toast.makeText(DetailsActivity.this, getResources().getString(R.string.afternow_text), Toast.LENGTH_LONG).show();
            return false;
        }
        else if(pretimeCheck()==false){ // if the days compatible with pretime setting
            Toast.makeText(DetailsActivity.this, getResources().getString(R.string.pretimecheck_text), Toast.LENGTH_LONG).show();
            //better give them a choice to go to the setting page
            return false;
        }
        else {

            return true;
        }
    }

    public Boolean pretimeCheck (){
        Calendar alarm_start_time = Calendar.getInstance();
        alarm_start_time.setTimeInMillis(selectedTimeE.getTimeInMillis()-daysOfPretimeE*24*60*60*1000);
        if(now.compareTo(alarm_start_time)>=0) return false;
        else return true;
    }

    public String getSelectedTime(){
        String selectedString = Integer.toString(selectedTimeE.get(Calendar.HOUR))+":"+Integer.toString(selectedTimeE.get(Calendar.MINUTE))
                + " "  + Integer.toString(selectedTimeE.get(Calendar.DAY_OF_MONTH))  + "/" + Integer.toString(selectedTimeE.get(Calendar.MONTH)
        )+ "/" +Integer.toString(selectedTimeE.get(Calendar.YEAR));
        return selectedString;
    }

    /**
     * Showing google speech input dialog for input name
     * */
    public void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (android.content.ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Showing google speech input dialog for input description
     * */
    public void promptSpeechInputDescription() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_DESCRIPTION);
        } catch (android.content.ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtTitleE.setText(result.get(0));
                }
                break;
            }
            case REQ_CODE_SPEECH_DESCRIPTION: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtDesE.setText(result.get(0));
                }
                break;
            }

        }
    }

}
