package com.example.aldrich.etimeapp;

/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
public class SettingClass {
    //claim properties
    private int s_id;
    private String s_pertime;
    private String s_frequency;
    private String s_alarm;
    public SettingClass(){

    }

    //set value to an setting
    public void setID(int id){this.s_id = id;}
    public void setPertime(String pertime){this.s_pertime = pertime;}
    public void setFrequency(String frequency){this.s_frequency = frequency;}
    public void setAlarm(String alarm){this.s_alarm= alarm;}

    //get value from a setting
    public int getID(){return this.s_id; }
    public String getPertime(){return this.s_pertime;}
    public String getFrequency(){return this.s_frequency;}
    public String getAlarm(){return this.s_alarm;}
}
