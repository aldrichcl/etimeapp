package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomDialog extends Dialog implements DialogInterface{

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }


    public static class Builder {
        //initialize the id of widgets
        Integer id_title;
        Integer id_btn_p;
        Integer id_btn_n;
        Integer id_message;
        Integer id_content;
        Integer id_ck;
        View layout;

        private Context context;
        private String title;
        private OnKeyListener mOnKeyListener;
        private String message;
        private OnDismissListener mOnDismissListener;
        private boolean mCancelable;
        private String positiveButtonText;
        private String negativeButtonText;
        private String checkboxText;
        private OnCancelListener onCancelListener;
        private View contentView;
        private DialogInterface.OnClickListener positiveButtonClickListener;
        private DialogInterface.OnClickListener negativeButtonClickListener;

        public Builder(Context context) {
            this.context = context;
        }
        public Builder setOnDismissListener(OnDismissListener onDismissListener) {
            this.mOnDismissListener = onDismissListener;
            return this;
        }
        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param title
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        /**
         * Set the Dialog title from resource
         *
         * @param title
         * @return
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        /**
         * Set the Dialog title from String
         *
         * @param title
         * @return
         */

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        //check box
        public Builder setCheckBox(String text) {
            this.checkboxText = text;
            return this;
        }
        public Builder setCheckBox(int text) {
            this.positiveButtonText = (String) context
                    .getText(text);
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText,
                                         DialogInterface.OnClickListener listener) {
            this.positiveButtonText = (String) context
                    .getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }


        public Builder setPositiveButton(String positiveButtonText,
                                         DialogInterface.OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText,
                                         DialogInterface.OnClickListener listener) {
            this.negativeButtonText = (String) context
                    .getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        //return if the checkbox is selected
        public boolean ifChecked(){
            CheckBox ck = (CheckBox)layout.findViewById(id_ck);
            if(checkboxText!=null && ck.isChecked()==true)return true;
            else return false;
        }

        public Builder setNegativeButton(String negativeButtonText,
                                         DialogInterface.OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        //create dialog
        public CustomDialog create(Boolean onclick, Integer theme) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomDialog dialog = new CustomDialog(context,R.style.Dialog);
            layout = inflater.inflate(R.layout.dialog_original, null);
            //check which theme is in use
            if(theme == 0){ //original theme
                layout = inflater.inflate(R.layout.dialog_original, null);
                 id_title = R.id.title;
                 id_btn_p= R.id.positiveButton;
                 id_btn_n= R.id.negativeButton;
                 id_message= R.id.message;
                 id_content= R.id.content;
                 id_ck = R.id.checkBox0;
            }
            else if (theme == 1) //natural theme
            {
                layout = inflater.inflate(R.layout.dialog_natural, null);
                id_title = R.id.title1;
                id_btn_p= R.id.positiveButton1;
                id_btn_n= R.id.negativeButton1;
                id_message= R.id.message1;
                id_content= R.id.content1;
                id_ck = R.id.checkBox1;
            }
            else if (theme == 2) //dark theme
            {
                layout = inflater.inflate(R.layout.dialog_dark, null);
                id_title = R.id.title2;
                id_btn_p= R.id.positiveButton2;
                id_btn_n= R.id.negativeButton2;
                id_message= R.id.message2;
                id_content= R.id.content2;
                id_ck = R.id.checkBox2;
            }
            else{
                layout = inflater.inflate(R.layout.dialog_original, null);
                id_title = R.id.title;
                id_btn_p= R.id.positiveButton;
                id_btn_n= R.id.negativeButton;
                id_message= R.id.message;
                id_content= R.id.content;
                id_ck = R.id.checkBox0;
            }

            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title
            ((TextView) layout.findViewById(id_title)).setText(title);

            //set the checkbox
            if (checkboxText != null) {
                ((CheckBox) layout.findViewById(id_ck))
                        .setText(checkboxText);
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(id_ck).setVisibility(
                        View.GONE);
            }
            // set the confirm button
            if (positiveButtonText != null) {
                ((Button) layout.findViewById(id_btn_p))
                        .setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    ((Button) layout.findViewById(id_btn_p))
                            .setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    positiveButtonClickListener.onClick(dialog,
                                            DialogInterface.BUTTON_POSITIVE);



                                }
                            });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(id_btn_p).setVisibility(
                        View.GONE);
            }
            // set the cancel button
            if (negativeButtonText != null) {
                ((Button) layout.findViewById(id_btn_n))
                        .setText(negativeButtonText);
                if (negativeButtonClickListener != null) {
                    ((Button) layout.findViewById(id_btn_n))
                            .setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    negativeButtonClickListener.onClick(dialog,
                                            DialogInterface.BUTTON_NEGATIVE);
                                }
                            });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(id_btn_n).setVisibility(
                        View.GONE);
            }
            // set the content message
            if (message != null) {
                ((TextView) layout.findViewById(id_message)).setText(message);
            } else if (contentView != null) {
                // if no message set
                // add the contentView to the dialog body
                ((LinearLayout) layout.findViewById(id_content))
                        .removeAllViews();
                ((LinearLayout) layout.findViewById(id_content))
                        .addView(contentView, new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
            }
            dialog.setContentView(layout);
            dialog.setCancelable(onclick);
            return dialog;
        }
    }
}