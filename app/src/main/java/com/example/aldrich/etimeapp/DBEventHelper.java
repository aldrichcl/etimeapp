package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Activity;
import android.app.usage.UsageEvents;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/23.
 */
public class DBEventHelper extends Activity{
    DBOpenHelper1 dbOpenHelper;
    public DBEventHelper(Context context){
        this.dbOpenHelper=new DBOpenHelper1(context);
    }

    /**
     * @param event
     */
    //add event to database
    public void addEvent(EventClass event){
        SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        //put content value
        cv.put("name", event.getName());
        cv.put("description", event.getDescription());
        cv.put("startdate", event.getStartDate());
        cv.put("starttime", event.getStartTime());
        cv.put("uporpast", event.getStatus());
        cv.put("alarmID", event.getAlarmID());
        cv.put("categoryID", event.getCategoryID());
        cv.put("pertime", event.getPretime());
        cv.put("frequency", event.getFrequency());

        /* insert data */
        db.insert("event", null, cv);
        db.close();

    }

    //get an event according to given alarmID
    public EventClass getEvent(String AlarmID){ //Note: alarmID is a String type data
        SQLiteDatabase db=dbOpenHelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select * from event where alarmID=?", new String[]{AlarmID});
        if(cursor.moveToFirst()){
            boolean a;
            if(cursor.getInt(cursor.getColumnIndex("uporpast"))==0){a=false;}
            else {a = true;
            }
            EventClass event= new EventClass();//initialize a new event
            //set value to the event
            event.setID(cursor.getInt(cursor.getColumnIndex("eventID")));
            event.setName(cursor.getString(cursor.getColumnIndex("name")));
            event.setDescription(cursor.getString(cursor.getColumnIndex("description")));
            event.setStartDate(cursor.getString(cursor.getColumnIndex("startdate")));
            event.setStartTime(cursor.getString(cursor.getColumnIndex("starttime")));
            event.setStatus(a);
            event.setCategoryID(cursor.getInt(cursor.getColumnIndex("categoryID")));
            event.setAlarmID(cursor.getString(cursor.getColumnIndex("alarmID")));
            event.setPretime(cursor.getInt(cursor.getColumnIndex("pertime")));
            event.setFrequency(cursor.getInt(cursor.getColumnIndex("frequency")));
            return event; //return the event
        }
        cursor.close();
        return null;
    }

    //modify a event by
    //1.create an event     --EventClass event
    //2.set the event according to the event's alarmID which is unique ID --event.getEvent(alarmID)
    //3.change the columns values  ----event.set***();
    //4.updateEvent(event)
    public void updateEvent(EventClass event){
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", event.getName());
        cv.put("description", event.getDescription());
        cv.put("startdate", event.getStartDate());
        cv.put("starttime", event.getStartTime());
        cv.put("uporpast", event.getStatus());
        cv.put("alarmID", event.getAlarmID());
        cv.put("categoryID", event.getCategoryID());
        cv.put("pertime", event.getPretime());
        cv.put("frequency", event.getFrequency());
        db.update("event", cv, "alarmID=" + event.getAlarmID(), null);
        db.close();
    }


    public void updateEventByeventID(EventClass event){
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", event.getName());
        cv.put("description", event.getDescription());
        cv.put("startdate", event.getStartDate());
        cv.put("starttime", event.getStartTime());
        //cv.put("uporpast", event.getStatus());
        cv.put("alarmID", event.getAlarmID());
        cv.put("categoryID", event.getCategoryID());
        cv.put("pertime", event.getPretime());
        cv.put("frequency", event.getFrequency());
        db.update("event", cv, "eventID=" + event.getID(), null);
        db.close();
    }

    //similar to update event, but requires insert all arguments
    public void modifyEvent(int id, String name, String description, String startdate, String starttime, int uporpast, int categoryid){
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put("name", name);
        args.put("description", description);
        args.put("startdate", startdate);
        args.put("starttime", starttime);
        args.put("uporpast", uporpast);
        args.put("categoryid", categoryid);

        db.update("event", args, "eventID=" + id, null);
        db.close();
    }

    //Note: all autoincrement ID start from 1, NOT zero
    public void deleteEvent(int index){
        SQLiteDatabase db=dbOpenHelper.getWritableDatabase();
        db.execSQL("DELETE FROM event WHERE eventID = " + Integer.toString(index));
        db.close();
    }

    //getCursor from database
    public Cursor getCursor() {
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor cur = null;
        cur = db.query("event", new String[]{"eventID" + " as _id", "name", "description", "starttime", "startdate", "uporpast", "categoryID"}, null, null, null, null, null);
        return cur;
    }
        /**
         * @param
         */

        //tai
        public Cursor getCursoronEventID(int eventID) {
            SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
            Cursor cur = null;
            cur =  db.rawQuery( "select name,description,startdate,starttime,categoryID,alarmID,pertime,frequency,uporpast from event where eventID="+eventID, null );
            cur.moveToFirst();
            return cur;
        }


}
