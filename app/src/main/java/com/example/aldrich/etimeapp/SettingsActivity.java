package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AppKeyPair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.UUID;

public class SettingsActivity extends ActionBarActivity {
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private Vibrator vibrator;

    //theme in use
    //0:original
    //1: natural
    //2:dark
    int theme;

    Spinner spnTheme;
    Spinner spnLanguage;
    Spinner  spn_alarm;
    private TextView tv;

    private String str_alarm;
    Button btnSave;
    Button btnDefault;
    Button btnPlay;
    Button btnBackup;
    Button btnRestore;
    Button btn_vib;
    Button btn_play;

    DBSettingHelper dbSettingHelper = new DBSettingHelper(this);
    SettingClass setting = new SettingClass();

    TextView textTitle;

    //for backup and restore
    String accessToken = null;
    final static private String APP_KEY = "8qgsq3aypf1jgb6";
    final static private String APP_SECRET = "jygf6u9rtwatsvf";
    private DropboxAPI<AndroidAuthSession> mDBApi;
    public String uniqueID = null;
    public boolean ifExist = false;

    private final static String DATABASE_NAME   = "TestDatabaseV6.db";
    public static final String PACKAGE_NAME = "com.example.aldrich.etimeapp";
    public static final String DB_PATH = "/data"
            + Environment.getDataDirectory().getAbsolutePath() + "/"
            + PACKAGE_NAME;
    public static final String DB_LOCATION = DB_PATH + "/databases/" + DATABASE_NAME;
    AndroidAuthSession session;
    String revNumber = "0"; //give a whatever value to initialize, better not to be null
    Boolean isSameUpload = false;

    //initialize preference
    SharedPreferences settings;
    SharedPreferences.Editor editor1;
    String vibrationStatus;
    Thread th_play;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Set to support only Portrait
        super.onCreate(savedInstanceState);

        // Get a SharedPreferences and its values
        final SharedPreferences preferences = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        theme = preferences.getInt("theme", 0);
        final int language = preferences.getInt("language", 0);

        // Set the language for the activity
        if (language == 0){
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            setTitle("Settings");
        }
        else if (language == 1){
            String languageToLoad  = "zh";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("设置");
        }
        else if(language == 2){
            String languageToLoad  = "vi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Cài đặt");
        }
        else if (language == 3){
            String languageToLoad  = "in";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Pengaturan");
        }
        else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Settings");
        }

        setContentView(R.layout.activity_settings);

        //get preference of setting
        settings = getSharedPreferences("my_settings", 0);
        editor1 = settings.edit(); //for vibrator
        btn_vib = (Button)findViewById(R.id.btn_vibration);
        //preference of vibration
        vibrationStatus = settings.getString("vibration", "on");
        //set button style according to theme currently in use
        if(vibrationStatus.equals("on")) {
            btn_vib.setText(SettingsActivity.this.getText(R.string.btn_vibration_off));
            btn_vib.setBackground(getResources().getDrawable(R.drawable.reddesignbutton));
        }
        else {
            btn_vib.setText(SettingsActivity.this.getText(R.string.btn_vibration_on));
            if (theme == 0){
                btn_vib.setBackground(getResources().getDrawable(R.drawable.designbutton));
            }
            else if (theme == 1){
                btn_vib.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            }
            else if (theme == 2){
                btn_vib.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            }
            else {
                btn_vib.setBackground(getResources().getDrawable(R.color.main_color));
            }
        }

        //initialization for backup and restore
        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        session = new AndroidAuthSession(appKeys);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);
        accessToken = "Hts9VBG4XhAAAAAAAAAAH4SbkVa5I-e6hr9FO7xAvB6HivR5NdRdAloBGhENeLYK";
        uniqueID = getUniquePsuedoID(); //get the unique ID as the user's ID of this device

        // Initialise widgets
        spn_alarm = (Spinner) findViewById(R.id.spnAlarm);
        spnTheme = (Spinner) findViewById(R.id.spn_theme);
        spnLanguage = (Spinner) findViewById(R.id.spn_language);
        btnSave= (Button)findViewById(R.id.button4);
        btnDefault = (Button) findViewById(R.id.button5);
        btnPlay = (Button) findViewById(R.id.btn_play);
        textTitle = (TextView) findViewById(R.id.settingspage_title);
        btnBackup = (Button) findViewById(R.id.btn_backup);
        btnRestore = (Button) findViewById(R.id.btn_restore);
        btn_play = (Button) findViewById(R.id.btn_play);

        // Set the theme to user's choice or default
        if (theme == 0){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            spnTheme.setSelection(0);
        }
        else if (theme == 1){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.natural_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.natural_background_color));
            textTitle.setTextColor(getResources().getColor(R.color.natural_main_color));
            btnSave.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnDefault.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnPlay.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnBackup.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnRestore.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            spnTheme.setSelection(1);
        }
        else if (theme == 2){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.dark_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.dark_background_color));
            textTitle.setTextColor(getResources().getColor(R.color.white_color));
            btnSave.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnDefault.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnPlay.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnBackup.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnRestore.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            spnTheme.setSelection(2);
        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            spnTheme.setSelection(0);
        }

        // Set the selection of spnLanguage dropdownlist to default or user's selection
        if (language == 0){
            spnLanguage.setSelection(0);
        }
        else if (language == 1){
            spnLanguage.setSelection(1);
        }
        else if (language == 2){
            spnLanguage.setSelection(2);
        }
        else if (language == 3){
            spnLanguage.setSelection(3);
        }
        else {
            spnLanguage.setSelection(0);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true); // Support a back button to previous Activity




        if(dbSettingHelper.getCount()==0)  //if it's a first-time run
        {
            spn_alarm.setSelection(0,true);
            setting.setAlarm("default_music");
            dbSettingHelper.addDefaultSetting(setting);

        }

        else {  // otherwise, load setting from database
            setting = dbSettingHelper.getSetting();
            int index = 0;
            if(setting.getAlarm().equals("default_music")){index=0;}
            else if(setting.getAlarm().equals("wake_up")){index=1;}
            else if(setting.getAlarm().equals("exciting")){index=2;}
            else if(setting.getAlarm().equals("lyrical")){index=3;}
            spn_alarm.setSelection(index, true);
        }

        //save button function
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //save alarm music preference to database
                if(spn_alarm.getSelectedItemId()==0) {
                    str_alarm = "default_music";
                }
                else if(spn_alarm.getSelectedItemId()==1) {
                    str_alarm = "wake_up";
                }
                else if(spn_alarm.getSelectedItemId()==2) {
                    str_alarm = "exciting";
                }
                else if(spn_alarm.getSelectedItemId()==3) {
                    str_alarm = "lyrical";
                }

                //add to database
                setting.setAlarm(str_alarm);
                dbSettingHelper.save(setting);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.savesuccessfully_text),
                        Toast.LENGTH_SHORT).show();

                // Create a SharedPreferences.Editor to edit preferences
                SharedPreferences.Editor editor = preferences.edit();

                if (spnTheme.getSelectedItemId() == 0) {
                    editor.remove("theme");
                    editor.putInt("theme", 0);
                }
                else if (spnTheme.getSelectedItemId() == 1) {
                    editor.remove("theme");
                    editor.putInt("theme", 1);
                }
                else if (spnTheme.getSelectedItemId() == 2) {
                    editor.remove("theme");
                    editor.putInt("theme", 2);
                }

                //set language to preference
                if (spnLanguage.getSelectedItemId() == 0){
                    editor.remove("language");
                    editor.putInt("language", 0);
                }
                else if (spnLanguage.getSelectedItemId() == 1){
                    editor.remove("language");
                    editor.putInt("language", 1);
                }
                else if (spnLanguage.getSelectedItemId() == 2){
                    editor.remove("language");
                    editor.putInt("language", 2);
                }
                else if (spnLanguage.getSelectedItemId() == 3){
                    editor.remove("language");
                    editor.putInt("language", 3);
                }
                editor.commit(); // Commit editor
                editor1.commit(); //also commit
            }
        });
    }


    //onClick of all buttons
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btn_play: { //btn play music
                //playMusic();
                if(th_play!=null){
                    if(th_play.isAlive())th_play.interrupt();
                }

               th_play=  new Thread(new Runnable() {

                    @Override
                    public void run() {
                        System.out.println(Thread.currentThread().getId());
                        runOnUiThread(play);
                    }

                });
                th_play.start(); //start playing music
                break;
            }
            case R.id.button5: { // reset btn
                spn_alarm.setSelection(0,true);
                spnTheme.setSelection(0, true);
                spnLanguage.setSelection(0, true);
                break;
            }
            case R.id.btn_backup: { //btn backup
                backup();
                break;
            }
            case R.id.btn_restore: { //btn restore
                restore();
                break;
            }
            case R.id.btn_vibration:{ //btn vibration
                Vibration();
                break;
            }
            default:
                break;
        }
    }
    Runnable play = new Runnable() {
        @Override
        public void run() { //runnable play music
            if(btn_play.getText().toString().equals(SettingsActivity.this.getText(R.string.btn_testMusic))){
                if(mediaPlayer.isPlaying()==true){mediaPlayer.stop();}
                if(spn_alarm.getSelectedItemId()==0) {
                    mediaPlayer = mediaPlayer.create(SettingsActivity.this,R.raw.default_music);
                    mediaPlayer.setLooping(false);
                    mediaPlayer.start();
                }
                else if(spn_alarm.getSelectedItemId()==2) {
                    mediaPlayer = mediaPlayer.create(SettingsActivity.this,R.raw.exciting);
                    mediaPlayer.setLooping(false);
                    mediaPlayer.start();
                }
                else if(spn_alarm.getSelectedItemId()==3) {
                    mediaPlayer = mediaPlayer.create(SettingsActivity.this,R.raw.lyrical);
                    mediaPlayer.setLooping(false);
                    mediaPlayer.start();
                }
                else if(spn_alarm.getSelectedItemId()==1) {
                    mediaPlayer = mediaPlayer.create(SettingsActivity.this,R.raw.wake_up);
                    mediaPlayer.setLooping(false);
                    mediaPlayer.start();
                }
                btn_play.setText(SettingsActivity.this.getText(R.string.btn_vibration_off)); //now is playing
                btn_play.setBackground(getResources().getDrawable(R.drawable.reddesignbutton));
            }
            else{
                if(mediaPlayer.isPlaying()==true){mediaPlayer.stop();}
                btn_play.setText(SettingsActivity.this.getText(R.string.btn_testMusic));
                //set color
                if (theme == 0){
                    btn_play.setBackground(getResources().getDrawable(R.drawable.designbutton));
                }
                else if (theme == 1){
                    btn_play.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
                }
                else if (theme == 2){
                    btn_play.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
                }
                else {
                    btn_vib.setBackground(getResources().getDrawable(R.color.main_color));
                }
            }
        }
    };


    public void Vibration(){ //vibration function
        if(btn_vib.getText().toString().equals(SettingsActivity.this.getText(R.string.btn_vibration_on))){
            btn_vib.setText(SettingsActivity.this.getText(R.string.btn_vibration_off)); //now vibration is on
            btn_vib.setBackground(getResources().getDrawable(R.drawable.reddesignbutton));
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.vibration_is_on),Toast.LENGTH_SHORT).show();
            //give a vibration
            vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            long [] pattern = {100,400,100,400};   // off on off on
            vibrator.vibrate(pattern,-1); // 2 means vibrate 2 times
            editor1.putString("vibration", "on");
        }
        else{
            btn_vib.setText(SettingsActivity.this.getText(R.string.btn_vibration_on));//now vibration is off
            //set color
            if (theme == 0){
                btn_vib.setBackground(getResources().getDrawable(R.drawable.designbutton));
            }
            else if (theme == 1){
                btn_vib.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            }
            else if (theme == 2){
                btn_vib.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            }
            else {
                btn_vib.setBackground(getResources().getDrawable(R.color.main_color));
            }
            editor1.putString("vibration", "off");
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.vibration_is_off),Toast.LENGTH_SHORT).show();

        }
    }


    Runnable upload = new Runnable() {
        @Override
        public void run() { //runnable upload for backup
            mDBApi.getSession().setOAuth2AccessToken(accessToken);
            File file = new File(DB_LOCATION);
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(file);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            DropboxAPI.Entry response1 = null;
            try {
                response1 = mDBApi.putFileOverwrite("/database001/" + uniqueID + ".db", inputStream,
                        file.length(), null);
                if(revNumber.equals(response1.rev)){ //check if anything changed to backup
                    isSameUpload = true;
                }
                else {isSameUpload = false;}
                revNumber = response1.rev;
            } catch (DropboxException e) {
                e.printStackTrace();
            }
            Log.i("DbExampleLog", "The uploaded file's rev is: " + response1.rev);
        }
    };



    Runnable download = new Runnable() {
        @Override
        public void run() { //runnable download for restore
            File file = new File(DB_LOCATION);
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            DropboxAPI.DropboxFileInfo info = null;
            try {
                info = mDBApi.getFile("/database001/"+ uniqueID +".db", null, outputStream, null);
            } catch (DropboxException e) {
                e.printStackTrace();

            }
            Log.i("DbExampleLog", "The file's rev is: " + info.getMetadata().rev);
        }
    };

    public void backup(){ //backup
        //build a dialog asking for confirmation
        final CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.costdata));
        builder.setTitle("Backup Reminder");
        builder.setPositiveButton(getResources().getString(R.string.yes_text), new DialogInterface.OnClickListener(){//set button
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.backingup),Toast.LENGTH_SHORT).show();

                Thread th_u = new Thread(upload);
                th_u.start();
                while(th_u.isAlive()){
                    //do nothing but wait
                }
                if(isSameUpload == true){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.nochange), Toast.LENGTH_SHORT).show();
                }
                else{
                    //revNumber = response.rev;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.backup_suc), Toast.LENGTH_SHORT).show();
                }


            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel_text),new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //what to do
            }
        });
        Log.d("dddddd",String.valueOf(theme));
        builder.create(false,theme).show();
    }

    public void restore(){ //restore function
        //build a dialog asking for confirmation
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.overwrite));
        builder.setTitle("Restore Reminder");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){//set button
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.checking),Toast.LENGTH_SHORT).show();
                mDBApi.getSession().setOAuth2AccessToken(accessToken);
                //new Thread(check).start();
                Thread th_c = new Thread(check);
                th_c.start();
                while(th_c.isAlive()){
                }
                if(ifExist==true){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.downloading),Toast.LENGTH_SHORT).show();
                    Thread th_d = new Thread(download);
                    th_d.start();
                    while(th_d.isAlive()){
                        //do nothing but wait
                    }
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.restore_suc),Toast.LENGTH_SHORT).show();
                }
                else {Toast.makeText(getApplicationContext(), getResources().getString(R.string.nobackup),Toast.LENGTH_SHORT).show();}

            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //what to do
            }
        });
        Log.d("dddddd",String.valueOf(theme));
        builder.create(false,theme).show();
    }
    Thread check = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                mDBApi.metadata("/database001/"+uniqueID+".db" , 1, null, false, null);
                ifExist =true;
            } catch (DropboxException e) {
                e.printStackTrace();
                ifExist = false;
            }
        }
    }){
    };

    protected void onResume() {
        super.onResume();
        if (mDBApi.getSession().authenticationSuccessful()
                && !mDBApi.getSession().isLinked()) {
            mDBApi.getSession().finishAuthentication();
            String Token = mDBApi.getSession().getOAuth2AccessToken();
        }
    }

    public static String getUniquePsuedoID() {
        String serial = null;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        try {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();
            //if API > 9, use serial number
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //initialize the serial number
            serial = "serial"; // name it whatever
        }
        //a  15-digit number accumulated by combining the hardware piecies ID
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }
}
