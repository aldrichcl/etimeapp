package com.example.aldrich.etimeapp;
//http://www.androidhive.info/2016/02/android-how-to-integrate-google-admob-in-your-app/ ads in the adding page
//http://www.androidhive.info/2014/07/android-speech-to-text-tutorial/ speech to text

/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.speech.RecognizerIntent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class AddActivity extends ActionBarActivity {
    EventClass event = new EventClass(); //this for create an event as an object
    DBEventHelper dbEventHelper = new DBEventHelper(this); //create helper for event
    private AlarmManager am1 = null; // created the alarmManager
    private AlarmManager am2  = null; // created the alarmManager
    Calendar now = Calendar.getInstance();
    Calendar selectedTime = now;

    //all start value will be -1 unless it's set by user
    int year_start, month_start, day_start;
    int hour_start, minute_start;

    //define buttons
    Button buttonSave;
    Button btnVoice;
    Button btnVoiceDescription;
    Button buttonReset;
    Button startTime;
    Button startDate;
    //define spinner
    Spinner spn_category;
    Spinner spn_pretime;
    Spinner spn_frequency;
    //define edit text
    EditText et_name; //this is for EditText_name
    EditText et_description; //this is for EditText_description
    //textview
    TextView pageTitle;
    TextView pageName;
    TextView pageDescription;
    TextView pageTime;
    TextView pageCategory;
    TextView preTimeText;
    TextView frequencyText;

    final int REQ_CODE_SPEECH_INPUT = 1;
    final int REQ_CODE_SPEECH_DESCRIPTION = 2;

    public int theme = 0;
    static final int STARTDATE_DIALOG_ID = 0;
    static final int STARTTIME_DIALOG_ID = 1;
    int daysOfPretime; //how nay days pf pretime setting
    int hoursOfFrequency;//how nay days pf frequency setting

    //get preference
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    Integer alarmNum = null;//part of alarmID

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Set to support only Portrait

        // Get a SharedPreferences and its values
        final SharedPreferences preferences = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        theme = preferences.getInt("theme", 0);
        final int language = preferences.getInt("language", 0);

        //initiate other preference
        settings = getSharedPreferences("my_settings", 0);
        editor = settings.edit(); //for vibrator


        // Set the language for the activity
        if (language == 0){
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            setTitle("Add Events");
        }
        else if (language == 1){
            String languageToLoad  = "zh";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("添加事件");
        }
        else if(language == 2){
            String languageToLoad  = "vi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Thêm sự kiện");
        }
        else if (language == 3){
            String languageToLoad  = "in";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Tambah Acara");
        }
        else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Add Events");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        // Initialise widgets
        et_name = (EditText)findViewById(R.id.event_name) ;
        et_description = (EditText)findViewById(R.id.event_description) ;
        spn_category = (Spinner)findViewById(R.id.spn_category) ;
        spn_frequency = (Spinner)findViewById(R.id.spnFrequency) ;
        spn_pretime = (Spinner)findViewById(R.id.spnPreTime) ;
        spn_pretime.setSelection(5);
        spn_frequency.setSelection(3);
        startTime= (Button)findViewById(R.id.start_time);
        startDate= (Button)findViewById(R.id.start_date);
        btnVoice = (Button) findViewById(R.id.btnVoice);
        btnVoiceDescription = (Button) findViewById(R.id.btnVoiceDescription);
        buttonReset = (Button) findViewById(R.id.reset_event);
        buttonSave =(Button) findViewById(R.id.save_event);
        pageTitle = (TextView) findViewById(R.id.addpage_title);
        pageName = (TextView) findViewById(R.id.addpage_name);
        pageDescription = (TextView) findViewById(R.id.addpage_description);
        pageTime = (TextView) findViewById(R.id.addpage_time);
        pageCategory = (TextView) findViewById(R.id.addpage_category);
        preTimeText = (TextView) findViewById(R.id.pre_text);
        frequencyText = (TextView) findViewById(R.id.frequency_text);

        // Set the theme to user's choice or default
        if (theme == 0){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        }
        else if (theme == 1){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.natural_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.natural_background_color));
            pageTitle.setTextColor(getResources().getColor(R.color.natural_main_color));
            pageName.setTextColor(getResources().getColor(R.color.natural_main_color));
            pageDescription.setTextColor(getResources().getColor(R.color.natural_main_color));
            pageTime.setTextColor(getResources().getColor(R.color.natural_main_color));
            pageCategory.setTextColor(getResources().getColor(R.color.natural_main_color));
            preTimeText.setTextColor(getResources().getColor(R.color.natural_main_color));
            frequencyText.setTextColor(getResources().getColor(R.color.natural_main_color));
            btnVoice.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            btnVoiceDescription.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            startDate.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            startTime.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            buttonReset.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
            buttonSave.setBackground(getResources().getDrawable(R.drawable.naturaldesignbutton));
        }
        else if (theme == 2){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.dark_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.dark_background_color));
            pageTitle.setTextColor(Color.WHITE);
            pageName.setTextColor(Color.WHITE);
            pageDescription.setTextColor(Color.WHITE);
            pageTime.setTextColor(Color.WHITE);
            pageCategory.setTextColor(Color.WHITE);
            preTimeText.setTextColor(Color.WHITE);
            frequencyText.setTextColor(Color.WHITE);
            btnVoice.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            btnVoiceDescription.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            startDate.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            startTime.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            buttonReset.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
            buttonSave.setBackground(getResources().getDrawable(R.drawable.darkdesignbutton));
        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true); // Support a back button to previous Activity

        //initiate the alarmManager
        am1 = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
        am2=(AlarmManager)getSystemService(Activity.ALARM_SERVICE);

        //dialog listener
        showDialogOnButtonClick();

        //voice btn listner
        btnVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
        //voice btn listner
        btnVoiceDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInputDescription();
            }
        });
        //reset btn listner
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetField();
            }
        });

        //prepraring for ads view
        mAdView = (AdView) findViewById(R.id.adView);

        // Create an ad request. Check your logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);

    }

    // Remove all of the fields and set it to default value.
    void resetField(){
        et_name.setText("");
        et_description.setText("");
        startTime.setText(getResources().getString(R.string.selecttime_text));
        startDate.setText(getResources().getString(R.string.selectdate_text));
        spn_category.setSelection(0);

    }
    //get alarmNum which is saved in preference, this will be part of alarmID
    public String getAlarmNum(){
        alarmNum = Integer.parseInt(settings.getString("alarmNum", "0")) ;
        return String.valueOf(alarmNum);
    }

    //when button save is clicked
    public void btnSave (View v){
        if(ifFieldsFilled()==false){
            // do nothing
        }
        else{
            ifPretimeValid();
        }
    }

    //save function
    public void save(){
        now = Calendar.getInstance();// get current time
        //acquire the alarmID, which is generated according to the time
        //AlarmID will be used to cancel corresponding alarm
        String AlarmID = String.valueOf( now.get(Calendar.HOUR))+ String.valueOf( now.get(Calendar.MINUTE))
                + String.valueOf( now.get(Calendar.SECOND)) + getAlarmNum();

        //prevent lost of accuracy when alarmID is received by AlarmReceive when first digit is "0"
        AlarmID = String.valueOf(Long.parseLong(AlarmID));//this is for keeping accuracy when passing data
        //generate controllerID, for controlling the repetitive alarm
        String EventControllerID = String.valueOf(Integer.parseInt(AlarmID) + 1) ;

        //set data into event (name,description etc.)
        event.setName(et_name.getText().toString());
        event.setDescription(et_description.getText().toString());
        event.setStartDate(startDate.getText().toString());
        event.setStartTime(startTime.getText().toString());
        event.setStatus(true);  //set to upcoming event
        event.setAlarmID(AlarmID);
        event.setCategoryID((int) spn_category.getSelectedItemPosition());
        event.setPretime((int)spn_pretime.getSelectedItemPosition());
        event.setFrequency((int)spn_frequency.getSelectedItemPosition());

        ///Set the selected time
        selectedTime = Calendar.getInstance(); //first initiate it so the second and millisecond is same with now
        selectedTime.set(Calendar.YEAR, year_start);
        selectedTime.set(Calendar.MONTH, month_start-1);
        selectedTime.set(Calendar.DATE, day_start);
        selectedTime.set(Calendar.HOUR_OF_DAY, hour_start);
        selectedTime.set(Calendar.MINUTE, minute_start);
        selectedTime.set(Calendar.SECOND, 0);
        selectedTime.set(Calendar.MILLISECOND, 0);

        //command dataValidation
        if(DataValidation()==false){
            //do nothing, because all toast is made inside datavalidation()
        }
        else{
            //get arguments we need
            //alarmStartTime is the first time the alarm rings
            long alarmStartTime = selectedTime.getTimeInMillis() - daysOfPretime*24*60*60*1000;
            Log.d("dddd selected time is:",selectedTime.getTime().toString());
            Log.d("dddd now time is: ",now.getTime().toString());
            Log.d("dddd pretime is: ",String.valueOf(daysOfPretime) + " days");
            Log.d("dddd starTimeInMillis:",String.valueOf(alarmStartTime));
            Log.d("dddd nowTimeInMillis:",String.valueOf(now.getTimeInMillis()));
            Log.d("dddd alarm starts at:",String.valueOf((alarmStartTime - now.getTimeInMillis())/1000/60) + " minute later");

            //repeat time is how often the alarm rings
            long alarmRepeatTime = hoursOfFrequency * 60* 60 *1000;

            //set alarm which is repetitive;
            Intent intent_Alarm = new Intent(AddActivity.this, AlarmReceiver.class);
            intent_Alarm.putExtra("alarmID", AlarmID);
            intent_Alarm.putExtra("eventName", et_name.getText().toString());
            intent_Alarm.putExtra("eventTime", getSelectedTime());
            intent_Alarm.putExtra("eventTimeInMillis", selectedTime.getTimeInMillis()); //long
            intent_Alarm.putExtra("eventFrequency", alarmRepeatTime); //long
            intent_Alarm.putExtra("count", 0);
            PendingIntent pi = PendingIntent.getBroadcast(AddActivity.this, Integer.parseInt(AlarmID), intent_Alarm,PendingIntent.FLAG_UPDATE_CURRENT );
            am1.setExact(AlarmManager.RTC_WAKEUP, alarmStartTime, pi);

            //add event to database
            dbEventHelper.addEvent(event);// add the event to database
            alarmNum++;//record that one alarm is added in preference
            //change preference of alarmNum
            editor.putString("alarmNum",String.valueOf(alarmNum) );
            editor.commit();
            Toast.makeText(AddActivity.this, getResources().getString(R.string.savesuccessfully_text),Toast.LENGTH_SHORT).show();
            Toast.makeText(AddActivity.this, getResources().getString(R.string.alarmset_text), Toast.LENGTH_LONG).show();
            //reset everything
            reset();
        }
    }

    public void ifPretimeValid(){ //check if pretime > frequency
        daysOfPretime  = 0;
        if(spn_pretime.getSelectedItemId()==0){ daysOfPretime=30;}
        else if(spn_pretime.getSelectedItemId()==1){daysOfPretime=14;}
        else if(spn_pretime.getSelectedItemId()==2){daysOfPretime=7;}
        else if(spn_pretime.getSelectedItemId()==3){daysOfPretime=3;}
        else if(spn_pretime.getSelectedItemId()==4){ daysOfPretime=1;}
        else if(spn_pretime.getSelectedItemId()==5){daysOfPretime=0;}


        hoursOfFrequency = 0;
        if(spn_frequency.getSelectedItemId()==0){ hoursOfFrequency=7*24;}
        else if(spn_frequency.getSelectedItemId()==1){ hoursOfFrequency=3*24;}
        else if(spn_frequency.getSelectedItemId()==2){hoursOfFrequency=1*24;}
        else if(spn_frequency.getSelectedItemId()==3){ hoursOfFrequency=12;}

        //does the user want to check this, default option is yes if the frequency > pretime
        if(settings.getString("PreFreCheck","1").equals("1")){//if yes
            if(daysOfPretime * 24 < hoursOfFrequency ) { //if it's a valid input
                final CustomDialog.Builder builder = new CustomDialog.Builder(AddActivity.this);
                builder.setMessage(getResources().getString(R.string.freq_long_text));
                builder.setTitle("eTime Reminder");
                builder.setPositiveButton(getResources().getString(R.string.yes_text), new DialogInterface.OnClickListener() {//set button
                    public void onClick(DialogInterface dialog, int which) {
                        if (builder.ifChecked() == true) {
                            editor.putString("PreFreCheck", "0"); // 0 means don't check again
                            editor.commit();
                        }
                        dialog.dismiss();
                        save();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel_text), new DialogInterface.OnClickListener() {//set button
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCheckBox(getResources().getString(R.string.remember_text));
                builder.create(false, theme).show();//false means oncancelable = false
            }
            else{
                save();
            }
        }
        else {// no and not to ask them
            save(); //directly save
        }

    }

    //check if necessary field is filled
    public boolean ifFieldsFilled(){
        if(et_name.getText().toString().equals((""))){  //if name has been filled which is necessary to be filled
            Toast.makeText(AddActivity.this, getResources().getString(R.string.forgetfields_text), Toast.LENGTH_LONG).show();
            return false;
        }
        //check if the time has been selected, which is necessary to fill
        else if(startTime.getText().toString().equals (getResources().getString(R.string.selecttime_text)) || startDate.getText().toString().equals (getResources().getString(R.string.selectdate_text))){//if user hasn't select a time at all
            Toast.makeText(AddActivity.this, getResources().getString(R.string.forgetdate_text), Toast.LENGTH_LONG).show();
            return false;
        }
        else return true;
    }

    //btn rest
    public void btn_reset(View v){
        reset();
    }
    //reset method
    public void reset(){//reset everything after user saved one successfully
        et_name.setText("");
        et_description.setText("");
        startTime.setText(getResources().getString(R.string.selecttime_text));
        startDate.setText(getResources().getString(R.string.selectdate_text));
    }

    //to generate the selected time in a unique format
    // "Hour:Minute day/month/year"
    public String getSelectedTime(){
        String selectedString = Integer.toString(selectedTime.get(Calendar.HOUR))+":"+Integer.toString(selectedTime.get(Calendar.MINUTE))
                + " "  + Integer.toString(selectedTime.get(Calendar.DAY_OF_MONTH))  + "/" + Integer.toString(selectedTime.get(Calendar.MONTH)
        )+ "/" +Integer.toString(selectedTime.get(Calendar.YEAR));
        return selectedString;
    }

    //data validation method
    public boolean DataValidation(){
        if(now.compareTo(selectedTime)>=0){ //if the day set already past
            Toast.makeText(AddActivity.this, getResources().getString(R.string.afternow_text), Toast.LENGTH_LONG).show();
            return false;
        }
        else if(pretimeCheck()==false){ // if the days compatible with pretime setting
            return false;
        }
        else {

            return true;
        }
    }

    // check if select time is after now + pretime
    public Boolean pretimeCheck (){
        Calendar alarm_start_time = Calendar.getInstance(); //get current time
        alarm_start_time.setTimeInMillis(selectedTime.getTimeInMillis()-daysOfPretime*24*60*60*1000);
        if(now.compareTo(alarm_start_time)>=0) {
            Toast.makeText(AddActivity.this, getResources().getString(R.string.after_pretime_check), Toast.LENGTH_LONG).show();
            return false;
        }
        else return true;
    }

    // When the button clicked and shows a dialog for time or date selection
    public void showDialogOnButtonClick() {
        //initialize buttons
        startDate = (Button) findViewById(R.id.start_date);
        startTime = (Button) findViewById(R.id.start_time);
        //listener of date picker
        startDate.setOnClickListener(
                new View.OnClickListener() {
                    Calendar now = Calendar.getInstance();
                    @Override
                    public void onClick(View v) {
                        new DatePickerDialog(AddActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        year_start = year;
                                        month_start = monthOfYear + 1;
                                        day_start = dayOfMonth;


                                        startDate.setText(year_start + "/" + month_start + "/" + day_start);
                                    }
                                }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)).show();// 设置年,月,日

                    }
                });

        //listener of time picker
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog a = new TimePickerDialog(AddActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                hour_start = hourOfDay;
                                minute_start = minute;
                                startTime.setText(hour_start + ":" + minute_start);
                            }
                        }, now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE), true);
                a.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    /**
     * Showing google speech input dialog for input name
     * */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (android.content.ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Showing google speech input dialog for input description
     * */
    private void promptSpeechInputDescription() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_DESCRIPTION);
        } catch (android.content.ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    et_name.setText(result.get(0));
                }
                break;
            }
            case REQ_CODE_SPEECH_DESCRIPTION: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    et_description.setText(result.get(0));
                }
                break;
            }

        }
    }

}
