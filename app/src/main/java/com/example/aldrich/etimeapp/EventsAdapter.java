package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by aldrich on 4/28/16.
 */
public class EventsAdapter extends ArrayAdapter {
    private final Context context;
    private final List<String[]> values;


    public EventsAdapter(Context context, List<String[]> values)
    {
        super(context, R.layout.list_event, values);
        this.context = context;
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); // Create a layout inflater
        View rowView = inflater.inflate(R.layout.list_event, parent, false); // Create a view for the rows in the ListView

        // Initialize and connect with the UI widgets
        ImageView eventCategory = (ImageView) rowView.findViewById(R.id.event_category);
        TextView eventName = (TextView) rowView.findViewById(R.id.event_name);
        TextView eventTime = (TextView) rowView.findViewById(R.id.event_time);
        TextView eventDate = (TextView) rowView.findViewById(R.id.event_date);

        // Set the content of UI widgets

        if (values.get(position)[6].toString() == "0"){
            eventCategory.setImageResource(R.drawable.general);
        }
        else if (values.get(position)[6].toString() == "1"){
            eventCategory.setImageResource(R.drawable.holiday);
        }
        else if (values.get(position)[6].toString() == "2"){
            eventCategory.setImageResource(R.drawable.birthday);
        }
        else if (values.get(position)[6].toString() == "3"){
            eventCategory.setImageResource(R.drawable.assignment);
        }
        else{
            eventCategory.setImageResource(R.drawable.general);
        }

        eventName.setText(values.get(position)[1].toString());
        eventTime.setText(values.get(position)[3].toString() + ", ");
        eventDate.setText(values.get(position)[4].toString());


        return rowView; // Return the rowView
    }
}
