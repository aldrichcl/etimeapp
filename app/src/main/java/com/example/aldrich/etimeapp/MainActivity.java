package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    public final static Intent addActivityIntent = new Intent("com.example.aldrich.etimeapp.AddActivity");
    public final static Intent settingActivityIntent = new Intent("com.example.aldrich.etimeapp.SettingActivity");
    public final static Intent aboutActivityIntent = new Intent("com.example.aldrich.etimeapp.AboutActivity");

    // List of String Array
    List<String[]> myList = new ArrayList<String[]>();
    List<String[]> upcomingList = new ArrayList<String[]>();
    List<String[]> pastList = new ArrayList<String[]>();
    List<String[]> generalList = new ArrayList<String[]>();
    List<String[]> holidayList = new ArrayList<String[]>();
    List<String[]> birthdayList = new ArrayList<String[]>();
    List<String[]> assignmentList = new ArrayList<String[]>();
    List<String[]> searchList = new ArrayList<String[]>();

    DBOpenHelper1 dbOpenHelper1;

    ImageButton addButton;

    ListView EventList;
    DBSettingHelper dbSettingHelper = new DBSettingHelper(this);
    SettingClass setting = new SettingClass();
    DBEventHelper dbEventHelper = new DBEventHelper(this);
    EventClass event = new EventClass();
    Cursor cursor = null;

    // Date Format initialised
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    private boolean mSearchOpened;
    private String mSearchQuery;
    private Drawable mIconOpenSearch;
    private Drawable mIconCloseSearch;

    EditText searchBar;
    private MenuItem mSearchAction;

    int navigationSection = 0;

    // Create string variables
    public static final String PREFS_NAME = "PreferencesFile";

    private boolean activityCreated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Set to support only Portrait

        // Get a SharedPreferences and its values
        final SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
        final int theme = preferences.getInt("theme", 0);
        final int language = preferences.getInt("language", 0);

        // Set the style of the theme depending on the value of variable theme
        if (theme == 0){
            setTheme(R.style.AppTheme);
        }
        else if (theme == 1){
            setTheme(R.style.NaturalAppTheme);
        }
        else if (theme == 2){
            setTheme(R.style.DarkAppTheme);
        }
        else {
            setTheme(R.style.AppTheme);
        }

        // Set the language for the activity
        if (language == 0){
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        else if (language == 1){
            String languageToLoad  = "zh";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        else if(language == 2){
            String languageToLoad  = "vi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        else if (language == 3){
            String languageToLoad  = "in";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activityCreated = true;

        // Initialise widgets
        EventList = (ListView)findViewById(R.id.list_view);
        addButton = (ImageButton)findViewById(R.id.add_button);

        // Set the theme to user's choice or default
        if (theme == 0){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            addButton.setBackground(getResources().getDrawable(R.drawable.oval));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        }
        else if (theme == 1){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.natural_main_color)));
            addButton.setBackground(getResources().getDrawable(R.drawable.naturaloval));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.natural_background_color));
        }
        else if (theme == 2){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.dark_main_color)));
            addButton.setBackground(getResources().getDrawable(R.drawable.darkoval));
            EventList.setBackgroundColor(getResources().getColor(R.color.dark_background_color));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.dark_background_color));

        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            addButton.setBackground(getResources().getDrawable(R.drawable.oval));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        }


        // Set the reaction to addButton when it's clicked.
        // Reference: https://www.youtube.com/watch?v=mTv3JxdVb9o
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainActivity.addActivityIntent);
            }
        });

        cursor = dbEventHelper.getCursor(); // get the cursor for reading database purposes.


        // Check if cursor is empty or filled. If filled, it will query table, and if empty, it will create a toast.
        if (cursor != null && cursor.getCount() > 0) {

            queryTable(cursor);
        }
        else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.noevents_text),
                    Toast.LENGTH_LONG).show();
        }

        onNavigationDrawerItemSelected(0); // Call method



        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        // Getting the icons.
        mIconOpenSearch = getResources()
                .getDrawable(R.drawable.search);
        mIconCloseSearch = getResources()
                .getDrawable(R.drawable.close);

        if (mSearchOpened) {
            openSearchBar(mSearchQuery);
        }

        showItemClickListener(EventList);

        //initialise the setting page
        //it's possible user will go directly to add page
        //so have to initialise at the very start of using this app
        if(dbSettingHelper.getCount()==0)  //if it's a first-time run
        {
            setting.setPertime("3 days");
            setting.setFrequency("1 day");
            setting.setAlarm("default_music");
            dbSettingHelper.addDefaultSetting(setting);
        }

    }

    /* This method is called, so when the activity is resume back from SettingsActivity, the theme will match the user's theme and language preferences.
       Reference: http://stackoverflow.com/questions/4334805/resolving-if-theme-is-changed-for-the-activity
    */
    @Override
    protected void onResume() {
        super.onResume();

        if (activityCreated == true) {
            activityCreated = false;
        }
        else {
            finish();
            startActivity(getIntent());
        }

    }

    //  Get information from database and save it to myList array, and check if event is pass or upcoming.
    void queryTable(Cursor cursor){
        myList.clear();
        while (cursor.moveToNext())
        {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String desc = cursor.getString(cursor.getColumnIndex("description"));
            String startTime = cursor.getString(cursor.getColumnIndex("starttime"));
            String startDate = cursor.getString(cursor.getColumnIndex("startdate"));
            int upOrPast = cursor.getInt(cursor.getColumnIndex("uporpast"));
            int categoryID = cursor.getInt(cursor.getColumnIndex("categoryID"));
            //String alarmID = cursor.getString(cursor.getColumnIndex("alarmID"));

            Date date = new Date();
            String stringDateData = startDate + " " + startTime;

            // Check if event is pass or upcoming, and modify it on database.
            try {
                Date dateData = dateFormat.parse(stringDateData);

                if ( dateData.compareTo(date) > 0 )
                {
                    dbEventHelper.modifyEvent(id, name, desc, startDate, startTime, 1, categoryID);
                }
                else
                {
                    dbEventHelper.modifyEvent(id, name, desc, startDate, startTime, 0, categoryID);
                }
                upOrPast = cursor.getInt(cursor.getColumnIndex("uporpast"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String[] myStringArray = new String[7]; // Create a String Variable with a size of 3

            myStringArray[0] = String.valueOf(id); // Set the first element of myStringArray with value of id in string
            myStringArray[1] = name.toString(); // Set the second element of myStringArray with the value of singer
            myStringArray[2] = desc.toString(); // Set the third value of myStringArray with the value of song
            myStringArray[3] = startTime.toString();
            myStringArray[4] = startDate.toString();
            myStringArray[5] = String.valueOf(upOrPast);
            myStringArray[6] = String.valueOf(categoryID);
            //myStringArray[7] = alarmID.toString();

            myList.add(myStringArray); // add myStringArray to myList
        }
    }

    // Filter myList array to only events that are upcoming
    void getUpcomingTable(){
        upcomingList.clear();
        int i;

        for (i=0; i < myList.size(); i++){
            String[] tempArray = new String[7];
            tempArray = myList.get(i);

            if (tempArray[5] == "1"){
                upcomingList.add(tempArray);
            }
        }
    }

    // Filter myList array to only events that are past
    void getPastTable(){
        pastList.clear();
        int i;

        for (i=0; i < myList.size(); i++){
            String[] tempArray = new String[7];
            tempArray = myList.get(i);

            if (tempArray[5] == "0"){
                pastList.add(tempArray);
            }
        }
    }

    // Filter myList array to only upcoming events that category is general
    void getGeneralTable(){
        generalList.clear();
        int i;

        for (i=0; i < myList.size(); i++){
            String[] tempArray = new String[7];
            tempArray = myList.get(i);

            if (tempArray[6] == "0" && tempArray[5] == "1"){
               generalList.add(tempArray);
            }
        }
    }

    // Filter myList array to only upcoming events that category is holiday
    void getHolidayTable(){
        holidayList.clear();
        int i;

        for (i=0; i < myList.size(); i++){
            String[] tempArray = new String[7];
            tempArray = myList.get(i);

            if (tempArray[6] == "1" && tempArray[5] == "1"){
                holidayList.add(tempArray);
            }
        }
    }

    // Filter myList array to only upcoming events that category is birthday
    void getBirthdayTable(){
        birthdayList.clear();
        int i;

        for (i=0; i < myList.size(); i++){
            String[] tempArray = new String[7];
            tempArray = myList.get(i);

            if (tempArray[6] == "2" && tempArray[5] == "1"){
                birthdayList.add(tempArray);
            }
        }
    }

    // Filter myList array to only upcoming events that category is assignment
    void getAssignmentTable(){
        assignmentList.clear();
        int i;

        for (i=0; i < myList.size(); i++){
            String[] tempArray = new String[7];
            tempArray = myList.get(i);

            if (tempArray[6] == "3" && tempArray[5] == "1"){
                assignmentList.add(tempArray);
            }
        }
    }

    // Sorts the list in order of from future closest date to farthest date
    List<String[]> sortAscendingList(List<String[]> list){
        boolean flag = true;
        String[] temp;
        Date dateOne;
        Date dateTwo;

        while ( flag ){
            flag = false;
            for(int i=0;  i < list.size() - 1;  i++ )
            {
                String[] arrayOne = list.get(i);
                String[] arrayTwo = list.get(i+1);

                String stringDateOne = arrayOne[4] + " " + arrayOne[3];
                String stringDateTwo = arrayTwo[4] + " " + arrayTwo[3];

                try {
                    dateOne = dateFormat.parse(stringDateOne);
                    dateTwo = dateFormat.parse(stringDateTwo);
                    if ( dateOne.compareTo(dateTwo) > 0 )
                    {
                        temp = list.get(i);
                        list.set(i, list.get(i + 1));
                        list.set(i + 1, temp);
                        flag = true;
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }

        return list;
    }

    // Sorts the list in order of from past closest date to farthest date
    List<String[]> sortDescendingList(List<String[]> list){
        boolean flag = true;
        String[] temp;
        Date dateOne;
        Date dateTwo;

        while ( flag ){
            flag = false;
            for(int i=0;  i < list.size() -1;  i++ )
            {
                String[] arrayOne = list.get(i);
                String[] arrayTwo = list.get(i+1);

                String stringDateOne = arrayOne[4] + " " + arrayOne[3];
                String stringDateTwo = arrayTwo[4] + " " + arrayTwo[3];

                try {
                    dateOne = dateFormat.parse(stringDateOne);
                    dateTwo = dateFormat.parse(stringDateTwo);
                    if ( dateOne.compareTo(dateTwo) <= 0 )
                    {
                        temp = list.get(i);
                        list.set(i, list.get(i + 1));
                        list.set(i + 1, temp);
                        flag = true;
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }

        return list;
    }

    // Change content when NavigationDrawer item is changed.
    @Override
    public void onNavigationDrawerItemSelected(int position) {

        navigationSection = position;
        switch (position){
            case 0:
                getUpcomingTable();
                upcomingList = sortAscendingList(upcomingList);
                if (upcomingList.size() > 0)
                {
                    EventsAdapter adapter = new EventsAdapter(this, upcomingList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
            case 1:
                getPastTable();
                pastList = sortDescendingList(pastList);
                if (pastList != null)
                {

                    EventsAdapter adapter = new EventsAdapter(this, pastList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
            case 2:
                getGeneralTable();
                generalList = sortAscendingList(generalList);
                if (generalList != null)
                {

                    EventsAdapter adapter = new EventsAdapter(this, generalList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
            case 3:
                getHolidayTable();
                holidayList = sortAscendingList(holidayList);
                if (holidayList != null)
                {
                    EventsAdapter adapter = new EventsAdapter(this, holidayList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
            case 4:
                getBirthdayTable();
                birthdayList = sortAscendingList(birthdayList);
                if (birthdayList != null)
                {

                    EventsAdapter adapter = new EventsAdapter(this, birthdayList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
            case 5:
                getAssignmentTable();
                assignmentList = sortAscendingList(assignmentList);
                if (assignmentList != null)
                {

                    EventsAdapter adapter = new EventsAdapter(this, assignmentList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
            default:
                if (myList != null) {
                    myList = sortDescendingList(myList);
                    EventsAdapter adapter = new EventsAdapter(this, myList); // Create an adapter variable and initialize it.
                    EventList.setAdapter(adapter); // Set the adapter for listView with adapter
                }
                break;
        }
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }


    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                break;
            case 5:
                mTitle = getString(R.string.title_section5);
                break;
            case 6:
                mTitle = getString(R.string.title_section6);
                break;
        }
    }
    // Method when search bar is open
    // Reference: http://blog.lovelyhq.com/implementing-a-live-list-search-in-android-action-bar/
    private void openSearchBar(String queryText) {

        // Set custom view on action bar, to support the search bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.search_bar);

        // Do the search of user's input
        searchBar = (EditText) actionBar.getCustomView()
                .findViewById(R.id.searchBarField);
        searchBar.addTextChangedListener(new SearchWatcher());
        searchBar.setText(queryText);
        searchBar.requestFocus();

        // Change search icon
        mSearchAction.setIcon(mIconCloseSearch);
        mSearchOpened = true;

    }

    // Method when search bar is closed
    private void closeSearchBar() {

        // Remove custom view
        getSupportActionBar().setDisplayShowCustomEnabled(false);

        // Change search icon
        // Reference: http://blog.lovelyhq.com/implementing-a-live-list-search-in-android-action-bar/
        mSearchAction.setIcon(mIconOpenSearch);
        mSearchOpened = false;

        EventsAdapter adapter;

        // Set the adapter back to the previous list view before search
        switch (navigationSection){
            case 0:
                adapter = new EventsAdapter(MainActivity.this, upcomingList);
                EventList.setAdapter(adapter);
                break;
            case 1:
                adapter = new EventsAdapter(MainActivity.this, pastList);
                EventList.setAdapter(adapter);
                break;
            case 2:
                adapter = new EventsAdapter(MainActivity.this, generalList);
                EventList.setAdapter(adapter);
                break;
            case 3:
                adapter = new EventsAdapter(MainActivity.this, holidayList);
                EventList.setAdapter(adapter);
                break;
            case 4:
                adapter = new EventsAdapter(MainActivity.this, birthdayList);
                EventList.setAdapter(adapter);
                break;
            case 5:
                adapter = new EventsAdapter(MainActivity.this, assignmentList);
                EventList.setAdapter(adapter);
                break;
            default:
                adapter = new EventsAdapter(MainActivity.this, myList);
                EventList.setAdapter(adapter);
                break;
        }


    }

    // A class implement from TextWatcher use to detect when text is changed.
    private class SearchWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        // Search for results that match the user's input and display on list view
        @Override
        public void afterTextChanged(Editable editable) {
            mSearchQuery = searchBar.getText().toString();

            searchList.clear();

            for(int i = 0; i < myList.size(); i++) {
                String[] tempArray = myList.get(i);
                if(tempArray[1].toLowerCase().contains(mSearchQuery.toLowerCase())){
                    searchList.add(tempArray);
                }
            }

            EventsAdapter adapter = new EventsAdapter(MainActivity.this, searchList);
            EventList.setAdapter(adapter);

        }

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(MainActivity.settingActivityIntent);

        }
        else if (id == R.id.action_about) {
            startActivity(MainActivity.aboutActivityIntent);
        }

        // If action_search was pressed, then it will check whether the search bar is opened or not, if it's open, then it will close, and vice versa.
        // Reference: http://blog.lovelyhq.com/implementing-a-live-list-search-in-android-action-bar/
        if (id == R.id.action_search) {
            if (mSearchOpened) {
                closeSearchBar();
            } else {
                openSearchBar(mSearchQuery);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
    //tai
    private void showItemClickListener(final ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String[] rowview = (String[]) listView.getItemAtPosition(position);

                //take the value in the row of list
                String evid = rowview[0];
                Intent intent =
                        new Intent(getBaseContext(), DetailsActivity.class);
                intent.putExtra("eventid", evid);
                startActivity(intent);
            }
        });
    }
}
