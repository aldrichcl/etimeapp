package com.example.aldrich.etimeapp;

/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class AboutActivity extends ActionBarActivity {

    TextView aboutPageTitle;
    TextView referenceText;
    ListView legalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Set to support only Portrait

        // Get a SharedPreferences and its values
        final SharedPreferences preferences = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        final int theme = preferences.getInt("theme", 0);
        final int language = preferences.getInt("language", 0);

        // Set the language for the activity
        if (language == 0){
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            setTitle("About");
        }
        else if (language == 1){
            String languageToLoad  = "zh";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("关于");
        }
        else if(language == 2){
            String languageToLoad  = "vi";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Thông tin");
        }
        else if (language == 3){
            String languageToLoad  = "in";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("Tentang");
        }
        else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            setTitle("About");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // Initalise widgets
        aboutPageTitle = (TextView) findViewById(R.id.aboutpage_title);
        legalList = (ListView) findViewById(R.id.legalList);
        referenceText = (TextView) findViewById(R.id.reference_text);

        // Set the theme to user's choice or default
        if (theme == 0){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);

        }
        else if (theme == 1){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.natural_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.natural_background_color));
            aboutPageTitle.setTextColor(getResources().getColor(R.color.natural_main_color));
            referenceText.setTextColor(getResources().getColor(R.color.natural_main_color));
        }
        else if (theme == 2){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.dark_main_color)));
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.dark_background_color));
            aboutPageTitle.setTextColor(getResources().getColor(R.color.white_color));
            referenceText.setTextColor(getResources().getColor(R.color.natural_main_color));
        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color)));
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true); // Support a back button to previous Activity

        // Create an array list
        List<String[]> list = new ArrayList<String[]>();


        // Create String Arrays then add to list
        String[] legalOne = new String[]{"default_music.mp3", "ccComunity", "Ben Warga"};
        list.add(legalOne);

        String[] legalTwo = new String[]{"wake_up.mp3", "CC0", "Kevin MacLeod"};
        list.add(legalTwo);

        String[] legalThree = new String[]{"exciting.mp3", "CC0", "Kevin MacLeod"};
        list.add(legalThree);

        String[] legalFour = new String[]{"lyrical.mp3", "CC0", "Kevin MacLeod"};
        list.add(legalFour);

        LegalAdapter adapter= new LegalAdapter(AboutActivity.this, list); // Create an adapter variable and initialize it.
        legalList.setAdapter(adapter); // Set the adapter for listView with adapter

         legalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

             public void onItemClick(AdapterView<?> parentAdapter, View view, int position,
                                     long id) {

                 // Setting different actions on ListView

                 if (position == 0) {
                     Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://freemusicarchive.org/music/Ben_Warga/Franklin_Canyon_Comp")); // Create an intent that opens browser and shows the reference.
                     startActivity(browserIntent); // Open browserIntent
                 }
                 else if (position == 1) {
                     Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://freepd.com/Chill/Anjou")); // Create an intent that opens browser and shows the reference.
                     startActivity(browserIntent); // Open browserIntent
                 }
                 else if (position == 2) {
                     Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://freepd.com/Electronic/Overt%20Intimidation%20Loop")); // Create an intent that opens browser and shows the reference.
                     startActivity(browserIntent); // Open browserIntent
                 }
                 else if (position == 3) {
                     Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://freepd.com/Acoustic/Morning%20Snowflake")); // Create an intent that opens browser and shows the reference.
                     startActivity(browserIntent); // Open browserIntent
                 }

             }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
