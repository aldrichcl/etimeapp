package com.example.aldrich.etimeapp;
//http://www.tutorialspoint.com/android/android_push_notification.htm


/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import junit.framework.Test;
public class ClockActivity extends Activity{
    //initialize strings
    String AlarmName = null;
    String eventName = null;
    String eventTime = null;

    SharedPreferences settings1; //get preference
    String vibrationStatus; //check if the vibration is turned on or off
    DBEventHelper dbEventHelper = new DBEventHelper(this); //create helper for event
    EventClass event = new EventClass(); //initialize a new event
    DBSettingHelper dbSettingHelper = new DBSettingHelper(this);//initialize settingHelper
    SettingClass setting = new SettingClass();//initialize a new setting

    private MediaPlayer mediaPlayer = new MediaPlayer(); //initialize mediaPlayer
    private Vibrator vibrator;//initialize vibrator

    @Override
    public void onCreate(Bundle savedInstanceState) {
        final SharedPreferences preferences = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        final int theme = preferences.getInt("theme", 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock);

        Bundle extras = getIntent().getExtras();
        //whether vibration is turned on or off
        settings1 = getSharedPreferences("my_settings", 0);
        vibrationStatus = settings1.getString("vibration", "on");
        eventName = extras.getString("eventName");
        eventTime = extras.getString("eventTime");
        //if it is an alarm or a controler
        if (extras == null) {
        }
        //if this is an event alarm
        else if(getIntent().hasExtra("alarmID")){
            if(vibrationStatus.equals("on")) {//check if the vibration is on
                vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                long [] pattern = {100,400,100,400};   // off on off on
                vibrator.vibrate(pattern,0); // 0 means keep repeating until cancelation called
            }
            else {
                //do not vibrate
            }
            //check which alarm music is in use
            setting = dbSettingHelper.getSetting();
            AlarmName = setting.getAlarm();

            //play alarm music accordingly
            if(AlarmName.equals("default_music")){
                if(mediaPlayer.isPlaying()==true){mediaPlayer.stop();}
                mediaPlayer = mediaPlayer.create(ClockActivity.this,R.raw.default_music);
                mediaPlayer.setLooping(true);
                mediaPlayer.start();
            }
            else if(AlarmName.equals("wake_up")){
                if(mediaPlayer.isPlaying()==true){mediaPlayer.stop();}
                mediaPlayer = mediaPlayer.create(ClockActivity.this,R.raw.wake_up);
                mediaPlayer.setLooping(true);
                mediaPlayer.start();
            }
            else if(AlarmName.equals("exciting")){
                if(mediaPlayer.isPlaying()==true){mediaPlayer.stop();}
                mediaPlayer = mediaPlayer.create(ClockActivity.this,R.raw.exciting);
                mediaPlayer.setLooping(true);
                mediaPlayer.start();
            }
            else if(AlarmName.equals("lyrical")){
                if(mediaPlayer.isPlaying()==true){mediaPlayer.stop();}
                mediaPlayer = mediaPlayer.create(ClockActivity.this,R.raw.lyrical);
                mediaPlayer.setLooping(true);
                mediaPlayer.start();
            }

            createNotification();//create notification

            //build a dialog to remid user
            CustomDialog.Builder builder = new CustomDialog.Builder(ClockActivity.this);
            //set dialog title and message
            builder.setMessage(eventName + "\n" + eventTime);
            builder.setTitle("eTime Reminder");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){//set button
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mediaPlayer.stop();
                    if(vibrationStatus.equals("on")) {
                        vibrator.cancel(); //stop vibrator
                    }
                    ClockActivity.this.finish();
                }
            });
            builder.create(false,theme).show();
        }
    }
    //create the notification when the event remind user
    public void createNotification() {
        String tittle="Reminder";
        String subject=eventName;
        String body=eventTime;
        NotificationManager notif=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify=new Notification(R.drawable.ic_stat_name,tittle,System.currentTimeMillis());
        PendingIntent pending= PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0);

        notify.setLatestEventInfo(getApplicationContext(),subject,body,pending);
        notif.notify(0, notify);

    }
}