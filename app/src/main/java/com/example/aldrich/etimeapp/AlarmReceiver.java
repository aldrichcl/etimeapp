package com.example.aldrich.etimeapp;

/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver {
    //initialize extras
    Integer count;
    String alarmID = null;
    String name = null;
    String time = null;
    Long alarmStartTime = null;
    Long eventTimeInMillis = null;
    Long frequency = null;
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent i=new Intent(context, ClockActivity.class);

        //get value from extra
        count=intent.getExtras().getInt("count");
        name = intent.getExtras().getString("eventName");
        time = intent.getExtras().getString("eventTime");
        alarmStartTime = Calendar.getInstance().getTimeInMillis();
        eventTimeInMillis = intent.getExtras().getLong("eventTimeInMillis");
        frequency = intent.getExtras().getLong("eventFrequency");
        alarmID= intent.getExtras().getString("alarmID");

        //put extra to pass to ClockAcitivity
        i.putExtra("count",count);
        i.putExtra("alarmID",alarmID);
        i.putExtra("eventName",name);
        i.putExtra("eventTime",time);

        //start ClockActivity
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        check(context);
    }

    //check if another alarm is needed by comparing the time
    public void check(Context ctxt){
        AlarmManager am1 = (AlarmManager) ctxt.getSystemService(Context.ALARM_SERVICE);
        // if current time is before event time, then check how to set next alarm
        if(eventTimeInMillis>alarmStartTime) {

            if(eventTimeInMillis-alarmStartTime>frequency){
                //if a frequency is still allowed for set another alarm
                //update
                count++;
                alarmStartTime = alarmStartTime + frequency;

            }
            //otherwise, set the alarm directly to the event time
            else{
                //update
                count++;
                alarmStartTime = eventTimeInMillis;
            }

            //preparing for another alarm
            Intent intent_Alarm = new Intent(ctxt, AlarmReceiver.class);
            intent_Alarm.putExtra("alarmID", alarmID);
            intent_Alarm.putExtra("eventName", name);
            intent_Alarm.putExtra("eventTime", time);
            intent_Alarm.putExtra("alarmStartTime", alarmStartTime);//long
            intent_Alarm.putExtra("eventTimeInMillis", eventTimeInMillis); //long
            intent_Alarm.putExtra("eventFrequency", frequency); //long
            intent_Alarm.putExtra("count", count);

            PendingIntent pi = PendingIntent.getBroadcast(ctxt, Integer.parseInt(alarmID), intent_Alarm,PendingIntent.FLAG_UPDATE_CURRENT );
            am1.setExact(AlarmManager.RTC_WAKEUP, alarmStartTime, pi);
        }
        else {
            //do nothing
        }

    }


}