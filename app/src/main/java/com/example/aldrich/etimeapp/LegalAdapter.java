package com.example.aldrich.etimeapp;
/*
    App Name: eTime
    Students Username: aclarenc, ptduong, and yrq

    SIT305 Project
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by aldrich on 5/13/16.
 */
public class LegalAdapter extends ArrayAdapter {
    private final Context context;
    private final List<String[]> values;

    public LegalAdapter(Context context, List<String[]> values)
    {
        super(context, R.layout.list_event, values);
        this.context = context;
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); // Create a layout inflater
        View rowView = inflater.inflate(R.layout.list_legal, parent, false); // Create a view for the rows in the ListView

        // Initialize and connect with the UI widgets
        TextView fileName = (TextView) rowView.findViewById(R.id.file_name);
        TextView attribution = (TextView) rowView.findViewById(R.id.attribution_type);
        TextView authorName = (TextView) rowView.findViewById(R.id.author_name);

        // Set the content of UI widgets
        fileName.setText(values.get(position)[0].toString());
        attribution.setText(values.get(position)[1].toString());
        authorName.setText(values.get(position)[2].toString());

        return rowView; // Return the rowView
    }
}
